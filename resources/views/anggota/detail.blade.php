@extends('layout/masterLayout')

@section('container')

<div class="container">
	<div class="card mt-2">
	  	<div class="card-header bg-dark text-white">
	  		<h4>Detail User</h4>
	  	</div>
	  <div class="card-body">
	  	<div class="row">
		    <div class="card col-md-8 mr-4">
			  <div class="card-body">
			    <table class="table table hover table-bordered table-striped">
			    	
			    		<tr>
				    		<td>Nama</td>
				    		<td>US-AA0{{$anggota->id}}</td>
				    	</tr>
				    	<tr>
				    		<td>Nama</td>
				    		<td>{{$anggota->name}}</td>
				    	</tr>
				    	<tr>
				    		<td>Jenis Kelamin</td>
				    		<td>{{$anggota->gender}}</td>
				    	</tr>
				    	<tr>
				    		<td>Group</td>
				    		<td>{{$anggota->groupId}}</td>
				    	</tr>
				    	<tr>
				    		<td>Jumlah anggota per-Group</td>
				    		<td>{{$count}} Anggota</td>
				    	</tr>
				    	<tr>
				    		<td>Catatan</td>
				    		<td>{{$anggota->remarks}}</td>
				    	</tr>
				    	<tr>
				    		<td>tanggal bergabung</td>
				    		<td>{{$anggota->created_at}}</td>
				    	</tr>
			    </table>
			    <a href="/anggota/{{$anggota->id}}/edit" class="btn btn-success btn-sm mt-3 mr-2">Edit User</a>
			    <form action="/anggota/{{$anggota->id}}" method="post" class="d-inline">
	            @method('delete')
	            @csrf
	          	   <button type="submit" class="btn btn-danger btn-sm mt-3">Hapus</button>
	            </form>
			  </div>
			</div>
			<div class="card" style="width: 18rem;">
				<div class="card-header bg-dark text-white text-center">
				  Gambar Profil
				</div>
			  <div class="card-body text-center">
			    <img class="" width="250px" src="{{ url('/dir_image/'.$anggota->image) }}">
			  </div>
			  <div class="card-body">
			   
			  </div>
			</div>
	  	</div>
	  </div>
	</div>
</div>

@endsection


