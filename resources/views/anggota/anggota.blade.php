@extends('layout/masterLayout')

@section('container')

<div class="container">
	<h1>Daftar <small>Anggota</small></h1>
	<hr>
	<div class="card">
	  <div class="card-body">
	  	<a href="/anggota/create" class="btn btn-secondary btn-sm mb-2" style="float: right;">Tambah Anggota</a>
		<table id="table_id" class="display col-lg-12">
		    <thead>
		        <tr>
		        	<th class="user-tbl" style="width: 20px;">No</th>
		        	<th class="user-tbl">IC User</th>
		            <th class="user-tbl">Nama Anggota</th>
		            <th class="user-tbl">Jenis Kelamin</th>
		            <th class="user-tbl">Tanggal join</th>
		            <th class="user-tbl">Group</th>
		            <th class="user-tbl">Catatan</th>
		            <th class="user-tbl">Gambar</th>
		           
		        </tr>
		    </thead>
		    <tbody>

		    	<?php $i=1; foreach ($anggota as $row) : ?>
			    	<tr>
			    		<td class="user-tbl"><?php echo $i++; ?></td>
			    		<td class="user-tbl">US-AA0{{$row->id}}</td>
			    		<td class="user-tbl"><a href="/anggota/{{$row->id}}">{{$row->name}}</a></td>
			    		<td class="user-tbl">{{$row->gender}}</td>
			    		<td class="user-tbl">{{$row->created_at}}</td>
			    		<td class="user-tbl">{{$row->groupId}}</td>
			    		<td class="user-tbl">{{$row->remarks}}</td>
			    		<td class="user-tbl"><img class="img-user" src="{{ url('/dir_image/'.$row->image) }}" width="30px"></td>
			    	</tr>
			    <?php endforeach; ?>

		       
		    </tbody>
		</table>
	  </div>
	</div>
</div>

@endsection


