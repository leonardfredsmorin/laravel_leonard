@extends('layout/masterLayout')

@section('container')

<div class="container">
	<h2 class="text-center">halaman Edit</h2>
	<hr>
	<div class="col-lg-6">
		<div class="card">
		  <div class="card-body">
		    <form action="/group/{{$group->id}}" method="post">
		    @method('put')
			@csrf
			  <div class="form-group">
			    <label for="exampleInputEmail1">Nama</label>
			    <input type="text" name="name" class="form-control form-control-sm @error('name') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$group->name}}">
			    @error('name')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Jumlah User</label>
			    <input type="text" name="description" class="form-control form-control-sm" id="exampleInputdescription1" aria-describedby="descriptionHelp" value="{{$group->total}}" readonly>
			  </div>
			  <div class="form-group">
			  	<label for="exampleInputEmail1">tanggal</label>
			  	<input type="date" class="form-control form-control-sm @error('date') is-invalid @enderror" name="date">
			  	@error('date')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Catatan Group</label>
			    <textarea type="text" name="remarks" class="form-control form-control-sm @error('remarks') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$group->remarks}}"></textarea> 
			    @error('remarks')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
		  </div>
		</div>
	</div>
</div>

@endsection


